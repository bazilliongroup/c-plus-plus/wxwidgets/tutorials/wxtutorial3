#include "capp.h"
#include "id.h"
#include "MainWindow.h"

// As in all programs, there must be a "main" function. Under wxWidgets, 
// main is implemented inside the wxIMPLEMENT_APP() macro, which creates an application 
// instance of the specified class and starts running the GUI event loop. It is used simply as:
wxIMPLEMENT_APP(cApp);

cApp::cApp() 
{

}

cApp::~cApp()
{

}

// As mentioned above, wxApp::OnInit() is called upon startup and should be used to 
// initialize the program, maybe showing a "splash screen" and creating the main window 
// (or several). Frames are created hidden by default, to allow the creation of child windows 
// before displaying them. We thus need to explicitly show them. Finally, we return true from 
// this method to indicate successful initialization:
bool cApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
	//the _ tells wx widgets that the string needs to be translated. 
	MainWindow *main = new MainWindow(nullptr, window::id::MAINWINDOW, _("Main Window"));
	main->Show();
	return true;
}

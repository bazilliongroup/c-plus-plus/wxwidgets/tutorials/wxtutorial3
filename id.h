#ifndef _ID_H_
#define _ID_H_

namespace window	
{ 
	enum id
	{
		MAINWINDOW = wxID_HIGHEST + 1
	};
}

#endif
#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include "wx/wx.h"

class MainWindow: public wxFrame
{
public:
	MainWindow(wxWindow *parent,
		wxWindowID id,
		const wxString& title,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize,
		long style = wxDEFAULT_FRAME_STYLE,
		const wxString& name = wxASCII_STR(wxFrameNameStr));
	~MainWindow();
};

#endif

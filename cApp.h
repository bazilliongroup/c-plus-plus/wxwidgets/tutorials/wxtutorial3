#ifndef _CAPP_H_
#define _CAPP_H_

#include "wx/wx.h"

// Practically every app should define a new class derived from wxApp. 
// By overriding wxApp's OnInit() virtual method the program can be initialized, 
// e.g. by creating a new main window.
class cApp : public wxApp
{
public:
	cApp();
	bool OnInit();	//Our applications entry point.
	~cApp();

};

DECLARE_APP(cApp);		//wxGetApp();

#endif